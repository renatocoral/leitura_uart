#include <stdio.h>
#include <unistd.h>         //Used for UART
#include <fcntl.h>          //Used for UART
#include <termios.h>        //Used for UART
#include <string.h>

int envia_dado_uart(int uart, unsigned char *dado, int size){
    printf("Escrevendo caracteres na UART ...");
    int count = write(uart, dado, size);
    if (count < 0)
    {
        printf("UART TX error\n");
    return -1;
    }
    else
    {
        printf("escrito.\n");
    }
    return 0;
}

float recebe_float(int uart){
    // Read up to 255 characters from the port if they are there
    unsigned char rx_buffer[100];
    int rx_length = read(uart, (void*)rx_buffer, 10);      //Filestream, buffer to store in, number of $
    if (rx_length < 0)
    {
        printf("Erro na leitura.\n"); //An error occured (will occur if there are no bytes)
        return 0;
    }
    else if (rx_length == 0)
    {
        printf("Nenhum dado disponível.\n"); //No data waiting
        return 0;
    }
    else{
        float dado = 0;
        memcpy(&dado, &rx_buffer[0], 4);
        return dado;
    }
}

int main(int argc, const char * argv[]) {

    int uart0_filestream = -1;

    uart0_filestream = open("/dev/serial0", O_RDWR | O_NOCTTY | O_NDELAY);      //Open in non blocking read/write mode
    if (uart0_filestream == -1)
    {
        printf("Erro - Não foi possível iniciar a UART.\n");
    }
    else
    {
        printf("UART inicializada!\n");
    }    
    struct termios options;
    tcgetattr(uart0_filestream, &options);
    options.c_cflag = B9600 | CS8 | CLOCAL | CREAD;     //<Set baud rate
    options.c_iflag = IGNPAR;
    options.c_oflag = 0;
    options.c_lflag = 0;
    tcflush(uart0_filestream, TCIFLUSH);
    tcsetattr(uart0_filestream, TCSANOW, &options);

    unsigned char codigo_temperatura[5] = {0xA1, 1, 2, 3, 4};
    unsigned char codigo_potenciometro[5] = {0xA2, 1, 2, 3, 4};

    printf("Inicia leitura da UART\n");

    while(1){

        envia_dado_uart(uart0_filestream, codigo_potenciometro, 5);
        sleep(1);
        float potenciometro = recebe_float(uart0_filestream);
        printf("Potenciometro: %f\n", potenciometro);

        envia_dado_uart(uart0_filestream, codigo_temperatura, 5);
        sleep(1);
        float tmp = recebe_float(uart0_filestream);
        printf("Temperatura: %f\n", tmp);

    }

    close(uart0_filestream);
    return 0;
}
